server {
    server_name site.dev site.test site.localhost site.lh;
    root /var/www/public;

    location / {
        access_log off;
        add_header Cache-Control no-cache;
        expires 0s;
        try_files $uri /index.php?$query_string;
    }

    location = /favicon.ico {
            log_not_found off;
            access_log off;
    }

    location = /robots.txt {
            allow all;
            log_not_found off;
            access_log off;
    }

    location ~* ^/.well-known/ {
            allow all;
    }

    location ~ /vendor/.*\.php$ {
            deny all;
            return 404;
    }

    location ~* \.(js|css|png|jpg|jpeg|gif|ico|svg)$ {
            try_files $uri 404;
            expires max;
            log_not_found off;
    }

    location ~ \.php(/|$) {
        fastcgi_pass php-fpm:9000;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;
        fastcgi_intercept_errors on;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;
        fastcgi_param QUERY_STRING $query_string;
        fastcgi_param PATH_INFO $fastcgi_path_info;
        fastcgi_param HTTPS off;
        fastcgi_read_timeout 600;
    }

    error_log /var/log/nginx/vnc_error.log;
    access_log /var/log/nginx/vnc_access.log;
}

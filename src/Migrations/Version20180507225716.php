<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180507225716 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user_auth (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(25) NOT NULL, password VARCHAR(64) NOT NULL, is_active TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_825FFC90F85E0677 (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE auth_roles (user_id INT NOT NULL, role_id INT NOT NULL, INDEX IDX_7A1C7FB2A76ED395 (user_id), INDEX IDX_7A1C7FB2D60322AC (role_id), PRIMARY KEY(user_id, role_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_tokens (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, engine VARCHAR(64) NOT NULL, provider VARCHAR(64) NOT NULL, token VARCHAR(512) NOT NULL, INDEX IDX_CF080AB3A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_private_data (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, email VARCHAR(64) NOT NULL, first_name VARCHAR(64) NOT NULL, middle_name VARCHAR(64) NOT NULL, last_name VARCHAR(64) NOT NULL, avatar VARCHAR(64) NOT NULL, UNIQUE INDEX UNIQ_BDFDFBDFA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_roles (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(25) NOT NULL, UNIQUE INDEX UNIQ_54FCD59F5E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE auth_roles ADD CONSTRAINT FK_7A1C7FB2A76ED395 FOREIGN KEY (user_id) REFERENCES user_auth (id)');
        $this->addSql('ALTER TABLE auth_roles ADD CONSTRAINT FK_7A1C7FB2D60322AC FOREIGN KEY (role_id) REFERENCES user_roles (id)');
        $this->addSql('ALTER TABLE user_tokens ADD CONSTRAINT FK_CF080AB3A76ED395 FOREIGN KEY (user_id) REFERENCES user_auth (id)');
        $this->addSql('ALTER TABLE user_private_data ADD CONSTRAINT FK_BDFDFBDFA76ED395 FOREIGN KEY (user_id) REFERENCES user_auth (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE auth_roles DROP FOREIGN KEY FK_7A1C7FB2A76ED395');
        $this->addSql('ALTER TABLE user_tokens DROP FOREIGN KEY FK_CF080AB3A76ED395');
        $this->addSql('ALTER TABLE user_private_data DROP FOREIGN KEY FK_BDFDFBDFA76ED395');
        $this->addSql('ALTER TABLE auth_roles DROP FOREIGN KEY FK_7A1C7FB2D60322AC');
        $this->addSql('DROP TABLE user_auth');
        $this->addSql('DROP TABLE auth_roles');
        $this->addSql('DROP TABLE user_tokens');
        $this->addSql('DROP TABLE user_private_data');
        $this->addSql('DROP TABLE user_roles');
    }
}

<?php
/**
 * VindecaNaturalCancerul (vindecanaturalcancerul.ro)
 * @link https://github.com/Noramarth/vnc-GabiNanes.git for the canonical source repository
 * @copyright Copyright (c) 2010 - 2018. Noramarth (noramarth@aom.ro) aka Dan Radu Dragomir 
 * @licence https://www.freebsd.org/copyright/freebsd-license.html
 */

declare(strict_types=1);

namespace App\Entity\User;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="user_roles")
 * @ORM\Entity(repositoryClass="App\Repository\User\Roles")
 */
class Roles
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $name;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Roles
     */
    public function setId($id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Roles
     */
    public function setName($name): self
    {
        $this->name = $name;
        return $this;
    }


}
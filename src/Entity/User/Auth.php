<?php
/**
 * VindecaNaturalCancerul (vindecanaturalcancerul.ro)
 * @link https://github.com/Noramarth/vnc-GabiNanes.git for the canonical source repository
 * @copyright Copyright (c) 2010 - 2018. Noramarth (noramarth@aom.ro) aka Dan Radu Dragomir
 * @licence https://www.freebsd.org/copyright/freebsd-license.html
 */

declare(strict_types=1);

namespace App\Entity\User;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ApiResource()
 * @ORM\Table(name="user_auth")
 * @ORM\Entity(repositoryClass="App\Repository\User\Auth")
 */
class Auth implements UserInterface, \Serializable
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @var string
     */
    private $salt;

    /**
     * @ORM\OneToOne(targetEntity="PrivateData", mappedBy="user")
     */
    private $privateData;

    /**
     * @ORM\ManyToMany(targetEntity="Roles")
     * @ORM\JoinTable(
     *     name="auth_roles",
     *     joinColumns={
     *          @JoinColumn(name="user_id", referencedColumnName="id")
     *      },
     *     inverseJoinColumns={
     *          @JoinColumn(name="role_id", referencedColumnName="id")
     *      }
     * )
     */
    private $roles;

    /**
     * @ORM\OneToMany(targetEntity="AuthTokens", mappedBy="user")
     * @JoinTable(name="auth_tokens",
     *     joinColumns={
     *          @JoinColumn(name="user_id", referencedColumnName="id")
     *      },
     *     inverseJoinColumns={
     *          @JoinColumn(name="token_id", referencedColumnName="id", unique=true)
     *      }
     * )
     */
    private $tokens;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->salt = md5(uniqid('', true));
        $this->roles = new ArrayCollection();
        $this->tokens = new ArrayCollection();
        $this->privateData = new ArrayCollection();
    }

    /**
     * @return ArrayCollection|null
     */
    public function getRoles(): ?ArrayCollection
    {
        return $this->roles;
    }

    /**
     * @param $role
     * @return Auth
     */
    public function addRole($role): self
    {
        $this->roles->add($role);
        return $this;
    }

    /**
     * @return ArrayCollection|null
     */
    public function getPrivateData(): ?ArrayCollection
    {
        return $this->privateData;
    }

    /**
     * @param PrivateData $data
     * @return Auth
     */
    public function addPrivateData(PrivateData $data): self
    {
        $this->privateData->add($data);
        return $this;
    }

    /**
     *
     */
    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize(): ?string
    {
        return serialize([
            $this->id,
            $this->username,
            $this->password,
        ]);
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            ) = unserialize($serialized, ['allowed_classes' => false]);
    }

    /**
     * @return mixed
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Auth
     */
    public function setId($id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     * @return Auth
     */
    public function setUsername($username): self
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     * @return Auth
     */
    public function setPassword($password): self
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getisActive(): ?bool
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     * @return Auth
     */
    public function setIsActive($isActive): self
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @return mixed
     */
    public function getTokens()
    {
        return $this->tokens;
    }

    /**
     * @param mixed $tokens
     * @return Auth
     */
    public function setTokens($tokens)
    {
        $this->tokens = $tokens;
        return $this;
    }

    /**
     * @param $role
     * @return bool
     */
    public function hasRole($role)
    {
        return in_array($role, $this->roles);
    }

}
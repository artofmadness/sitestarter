<?php
/**
 * VindecaNaturalCancerul (vindecanaturalcancerul.ro)
 * @link https://github.com/Noramarth/vnc-GabiNanes.git for the canonical source repository
 * @copyright Copyright (c) 2010 - 2018. Noramarth (noramarth@aom.ro) aka Dan Radu Dragomir 
 * @licence https://www.freebsd.org/copyright/freebsd-license.html
 */

declare(strict_types=1);


namespace App\Entity\User;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;

/**
 * @ORM\Table(name="user_tokens")
 * @ORM\Entity(repositoryClass="App\Repository\User\AuthTokens")
 */
class AuthTokens
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Auth", inversedBy="tokens")
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $engine;
    /**
     * @ORM\Column(type="string", length=64)
     */
    private $provider;
    /**
     * @ORM\Column(type="string", length=512)
     */
    private $token;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return AuthTokens
     */
    public function setId($id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEngine(): ?string
    {
        return $this->engine;
    }

    /**
     * @param mixed $engine
     * @return AuthTokens
     */
    public function setEngine($engine): self
    {
        $this->engine = $engine;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProvider(): ?string
    {
        return $this->provider;
    }

    /**
     * @param mixed $provider
     * @return AuthTokens
     */
    public function setProvider($provider): self
    {
        $this->provider = $provider;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     * @return AuthTokens
     */
    public function setToken($token): self
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return AuthTokens
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }


}
<?php
/**
 * VindecaNaturalCancerul (vindecanaturalcancerul.ro)
 * @link https://github.com/Noramarth/vnc-GabiNanes.git for the canonical source repository
 * @copyright Copyright (c) 2010 - 2018. Noramarth (noramarth@aom.ro) aka Dan Radu Dragomir
 * @licence https://www.freebsd.org/copyright/freebsd-license.html
 */

declare(strict_types=1);


namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class Admin extends Controller
{

    /**
     * @Route(
     *     name="dashboard",
     *     path="/admin",
     *     methods={"GET"},
     *
     * )
     */
    public function dashboard()
    {
        return $this->render(
            'admin/dashboard.html.twig'
        );
    }
}
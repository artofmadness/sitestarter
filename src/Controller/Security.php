<?php
/**
 * VindecaNaturalCancerul (vindecanaturalcancerul.ro)
 * @link https://github.com/Noramarth/vnc-GabiNanes.git for the canonical source repository
 * @copyright Copyright (c) 2010 - 2018. Noramarth (noramarth@aom.ro) aka Dan Radu Dragomir
 * @licence https://www.freebsd.org/copyright/freebsd-license.html
 */


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class Security
 * @package App\Controller
 */
class Security extends Controller
{
    /**
     * @Route(
     *     name="login",
     *     path="/login",
     *     methods={"POST"}
     * )
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $array = array('success' => false, 'message' => $error);
        $response = new Response(json_encode($array));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
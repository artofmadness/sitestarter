<?php
/**
 * VindecaNaturalCancerul (vindecanaturalcancerul.ro)
 * @link https://github.com/Noramarth/vnc-GabiNanes.git for the canonical source repository
 * @copyright Copyright (c) 2010 - 2018. Noramarth (noramarth@aom.ro) aka Dan Radu Dragomir 
 * @licence https://www.freebsd.org/copyright/freebsd-license.html
 */


namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class Index extends Controller
{
    /**
     * @Route(
     *     name = "home",
     *     path = "/",
     *     methods = {"GET"}
     * )
     * @param AuthenticationUtils $utils
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function default(AuthenticationUtils $utils)
    {
        $error = $utils->getLastAuthenticationError();
        /** @var string $lastUsername */
        $lastUsername = $utils->getLastUsername();
        return $this->render('default/index.html.twig', [
            'title' => 'welcome',
            'last_username' => $lastUsername,
            'error' => $error
        ]);
    }
    /**
     * @Route(
     *     name = "info",
     *     path = "/test",
     *     methods = {"GET"}
     * )
     */
    public function info() {
        phpinfo();
    }
}
/*
 * VindecaNaturalCancerul (vindecanaturalcancerul.ro)
 * @link https://github.com/Noramarth/vnc-GabiNanes.git for the canonical source repository
 * @copyright Copyright (c) 2010 - 2018. Noramarth (noramarth@aom.ro) aka Dan Radu Dragomir
 * @licence https://www.freebsd.org/copyright/freebsd-license.html
 */

$("#myModal").on("shown.bs.modal", function () {
    $("#myInput").trigger("focus");
});

$('#login_submit_btn').on("click", function () {
    var username = $('#username').val();
    var password = $('#password').val();
    var data = JSON.stringify({_username: username, _password: password});
    $.ajax({
        type: "POST",
        url: "/login",
        dataType: "json",
        contentType: "application/json",
        data: data,
        success: function (response) {
            location.reload();
        },
        error: function (httpObj, textStatus) {
            $("#login_error").text(httpObj.responseJSON.error);
        }
    });
});
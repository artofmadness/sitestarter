var Encore = require("@symfony/webpack-encore");

Encore
    .setOutputPath("public/build/")
    .setPublicPath("/build")
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .enableSassLoader(function (sassOptions) {
    }, {
        resolveUrlLoader: false
    })
    .addEntry("global", "./assets/global.js")
    .addEntry("app_styles","./assets/styles.js")
    .addEntry("login","./assets/js/login.js")
    .autoProvidejQuery()
;

module.exports = Encore.getWebpackConfig();
